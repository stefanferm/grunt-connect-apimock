'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testAdvancedFormatGet(test, path, cookies, expectedStatus, expectedMessage, failMessage){
    test.expect(3);
    var requestOptions = {
      path: path,
      method: 'GET',
      port: 8080
    };
    if(cookies !== undefined){
      requestOptions.headers = {'Cookie': cookies};
    }
    http.request(requestOptions, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, expectedStatus);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var responseBody = JSON.parse(data);
        test.equal(responseBody.message, expectedMessage, failMessage);
        test.done();
      });
    }).end();
}

exports.connect_apimock = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },

  requestparameter_first_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameter?foo=foo',
      undefined,
      400,
      'foo',
      'requestparameter?foo=foo should return {"message":"foo"}');
  },
  requestparameter_second_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameter?foo=bar',
      undefined,
      200,
      'bar',
      'requestparameter?foo=bar should return {"message":"bar"}');
  },
  requestparameter_no_match1: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameter?foo=asdf',
      undefined,
      201,
      'foofoofoo',
      'requestparameter?foo=asdf should return {"message":"foofoofoo"}');
  },
  requestparameter_no_match2: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameter?bar=asdf',
      undefined,
      201,
      'foofoofoo',
      'requestparameter?bar=asdf should return {"message":"foofoofoo"}');
  },
  requestparameter_no_defaultresponse_no_match: function(test){
    test.expect(3);
    http.request({
      path: '/api/advanced/requestparameter_no_defaultresponse',
      method: 'GET',
      port: 8080
    }, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, 500);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var expected = '{"error":"No response could be found"}';
        test.equal(data, expected, 'should return an error');
        test.done();
      });
    }).end();
  },
  requestparameter_no_parameters: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameter_no_parameters',
      undefined,
      201,
      'foofoofoo',
      'should return {"message":"foofoofoo"}');
  },



  requestparameters_one_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameters?foo=bar',
      undefined,
      201,
      'foofoofoo',
      'requestparameters?foo=bar should return {"message":"foofoofoo"}');
  },
  requestparameters_both_matches: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/requestparameters?foo=bar&bar=foo',
      undefined,
      401,
      'foobar',
      'requestparameters?foo=bar&bar=foo should return {"message":"foobar"}');
  }


};
