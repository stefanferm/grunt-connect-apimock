'use strict';

var grunt = require('grunt');
var http = require('http');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

function testAdvancedFormatGet(test, path, cookies, expectedStatus, expectedMessage, failMessage){
    test.expect(3);
    var requestOptions = {
      path: path,
      method: 'GET',
      port: 8080
    };
    if(cookies !== undefined){
      requestOptions.headers = {'Cookie': cookies};
    }
    http.request(requestOptions, function(response) {
      var data = '';
      response.on('data', function (chunk) {
        data += chunk;
      });
      response.on('end', function () {
        test.equal(response.statusCode, expectedStatus);
        test.equal(response.headers['content-type'], 'application/json;charset=UTF-8');
        var responseBody = JSON.parse(data);
        test.equal(responseBody.message, expectedMessage, failMessage);
        test.done();
      });
    }).end();
}

exports.connect_apimock = {
  setUp: function (done) {
    // setup here if necessary
    done();
  },


  cookie_first_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/cookie',
      'foo=foo',
      400,
      'foo',
      'should return {"message":"foo"}');
  },
  cookie_second_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/cookie',
      'foo=bar',
      200,
      'bar',
      'should return {"message":"bar"}');
  },
  cookie_no_match: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/cookie',
      'asdf=asdf',
      201,
      'foofoofoo',
      'should return {"message":"foofoofoo"}');
  },
  cookie_no_cookies_in_file: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/cookie_no_cookies',
      'foo=foo',
      201,
      'foofoofoo',
      'should return {"message":"foofoofoo"}');
  },
  cookie_no_cookies_in_request: function(test){
    testAdvancedFormatGet(test,
      '/api/advanced/cookie',
      undefined,
      201,
      'foofoofoo',
      'should return {"message":"foofoofoo"}');
  }

};
